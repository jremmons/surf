function resultant_field = add_sfield(sfield, ifield, field, numerics, t)
        
    resultant_field = field + sfield .* exp( -i * ifield.w*t );

end