function phi = ie_solver(boundary, ifield, numerics)

    % assembly linear integral equation system
    [A, b] = ie_assemble(boundary, ifield, numerics);
    
    % solve the system
    phi = linsolve(A, b);

end