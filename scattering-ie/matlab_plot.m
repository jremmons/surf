function matlab_plot(boundary, field, numerics)

    hold on
    plot_field(field, numerics);
    plot_boundary(boundary);
    hold off

end