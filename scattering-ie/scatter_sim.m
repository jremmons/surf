%
% Author: John Emmons
%
close all;
clear all;

% define parameters
disp('Begin program')
disp('* set parameters')
boundary = init_boundary();
numerics = init_numerics();
ifield = init_ifield();

% solve integral equations
disp('* solve integral equations')
phi = ie_solver(boundary, ifield, numerics);

% find scattered field
disp('* find scattered field')
sfield = compute_sfield(boundary, ifield, phi, numerics);

disp('* begin time evolution plotting')
% writerObj = VideoWriter('movie.avi');
% writerObj.FrameRate = 20;
% writerObj.Quality = 75;
% open(writerObj);

% FigHandle = figure;
% set(FigHandle, 'Position', [100, 100, 1.5*700, 1.5*512]);

tic
idx_max = 100;
for idx=1:idx_max
    t_max = pi;
    t = (t_max/idx_max) * idx;

    % initialize zero field
    field = zeros(numerics.ymesh_dim, numerics.xmesh_dim);

    % add incident field
    field = add_ifield(ifield, field, numerics, t);

    % solve and add scattered field
    field = add_sfield(sfield, ifield, field, numerics, t);

    % mask out center in circular case
    %field = field .* (numerics.X.^2 + numerics.Y.^2 > 1);

    % output the field in tecplot format
    % fname = strcat('tec/field', int2str(idx));
    % write2tecplot(numerics.X, numerics.Y, real(field), fname);

    % output the field in silo format
    fname = strcat('silo/field', int2str(idx));
    silo2d(numerics.X, numerics.Y, real(field), fname);

    % plot the field and boundary using MATLAB
    % matlab_plot(boundary, field, numerics)
    % pause(0.01)

    % Set some conditions for initial frame
    % if idx == 1
    %     set(gca,'nextplot','replacechildren');
    %     set(gcf,'Renderer','zbuffer');
    % end
    % frame = getframe(gcf);
    % writeVideo(writerObj,frame);

end
toc
%close(writerObj);
disp('* end time evolution plotting')

disp('Program complete!')