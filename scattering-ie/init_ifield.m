function ifield = init_ifield()

    ifield.k = [-10, 0];
    ifield.w = 10;

    % e^( i(k*x - wt) ) 
    ifield.field = @(x, y ,t) exp( i*((ifield.k(1)*x + ifield.k(2)*y) ...
                                   - ifield.w*t) );

end