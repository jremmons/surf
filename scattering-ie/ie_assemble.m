function [A, b] = ie_assemble(boundary, ifield, numerics)

    [t_i, t_j] = meshgrid(boundary.t_space, boundary.t_space);

    % assemble R, M1, and M1 (used to get A
    R = zeros(boundary.t_dim);
    pi_n2   = pi / boundary.n^2;
    for m=1:(boundary.n-1)
        R = R + (1/m)*cos( m*(t_i - t_j) );
    end
    R = -((2*pi) / boundary.n) * R - pi_n2*cos( boundary.n*(t_i - t_j));

    [x_i, x_j] = meshgrid(boundary.X, boundary.X);
    [y_i, y_j] = meshgrid(boundary.Y, boundary.Y);
    r = sqrt( (x_i - x_j).^2 + (y_i - y_j).^2 );
    
    k = norm(ifield.k);
    M1 = (-1 / (4*pi)) * meshgrid(boundary.ds) .* besselj(0, k*r);

    M2 = zeros(boundary.t_dim);
    M2 = (i/4) * besselh(0, 1, k * r) .* meshgrid(boundary.ds) - M1 .* ...
         log( 4*sin((t_i - t_j) / 2).^2 );


    M2( eye(boundary.t_dim) == 1 ) = ...
        diag( meshgrid(boundary.ds) .* ...
             ( (i/4) - numerics.C/(2*pi) - ...
               (1/(2*pi)) * log( (k/2) * meshgrid(boundary.ds) ) ...
               ) ...
    );
        
    % assemble A
    A = R .* M1 + (pi/boundary.n) * M2;

    % assemble b
    b = transpose(double( -ifield.field( boundary.funx(boundary.t_space), ... 
                                        boundary.funy(boundary.t_space), ...
                                        0 ) ...
                          ));

end