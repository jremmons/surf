function resultant_field = add_ifield(ifield, field, numerics, t)
    
    resultant_field = field + ifield.field(numerics.X, numerics.Y, t);

end