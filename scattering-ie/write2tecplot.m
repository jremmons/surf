function res = write2tecplot( x, y, fxy, filename )
%  -----------------------------------------------------------------------------
%                       write2tecplot.m
%  -----------------------------------------------------------------------------
%  This saves a 2D curvilinear domain stored in the matrices X and Y, and a data
%  matrix FXY, of the same dimensions, defined over this domain.
%
%  Author: Edwin Jimenez 
%  Modified: 8 April 2014
%  -----------------------------------------------------------------------------

  D = size(x);

  imax = D(1);
  jmax = D(2);

  tecfile = fopen( [filename, '.tec'], 'w' );

  fprintf( tecfile, ' title = "2D Equation" \n' );
  fprintf( tecfile, ' variables = "x", "y", "u"\n' );
  fprintf( tecfile, ' zone i = %d, j = %d, DATAPACKING=POINT \n', imax, jmax );

  for j = 1:jmax,
  for i = 1:imax,
    fprintf( tecfile, '%10.4f %10.4f %10.4f \n', x(i,j), y(i,j), fxy(i,j) );
  end
  end

  fclose( tecfile );

%  -----------------------------------------------------------------------------
%  -----------------------------------------------------------------------------
