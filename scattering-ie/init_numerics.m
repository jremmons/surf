function numerics = init_numerics()

    numerics.C = 0.5772156649015328606065120900824024310421;

    % numerics for mesh in xy-plane
    numerics.xmin = -5;
    numerics.xmax = 5;
    numerics.ymin = -5;
    numerics.ymax = 5;

    numerics.xmesh_dim = 500;
    numerics.ymesh_dim = 500;

    numerics.x_space = linspace(numerics.xmin, numerics.xmax, ...
                                numerics.xmesh_dim);
    numerics.y_space = linspace(numerics.ymin, numerics.ymax, ...
                                numerics.ymesh_dim);

    [numerics.X, numerics.Y] = meshgrid(numerics.x_space, numerics.y_space);

end