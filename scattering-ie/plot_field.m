function plot_field(field, numerics)

    pcolor(numerics.x_space, numerics.y_space, real(field));
    caxis([-5,5])
    shading interp
    colormap hot
    colorbar
    axis square

end
