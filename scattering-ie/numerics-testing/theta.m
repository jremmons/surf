function t = theta(k, X, Y)
    
    t = [];
    for i_idx=1:length(X)
        for j_idx=1:length(Y)
            vec = [X(i_idx, j_idx), Y(i_idx, j_idx)];
            cosTheta = dot(k, vec) / (norm(k) * norm(vec));

            t(i_idx, j_idx) = acos(cosTheta);
        end
    end

end