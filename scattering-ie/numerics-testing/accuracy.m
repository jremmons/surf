%
% Author: John Emmons
%
clear all;
close all;

% define the points where we are computing the field
%n = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, ...
%     20, 25, 30, 35, 40, 45, 50, 100, 200, 500];

n = round( logspace(0,2, 10) );

net_error = [];
for boundary_points = n
    boundary = init_boundary(boundary_points);
    numerics = init_numerics();
    ifield = init_ifield();
    
    % compute the field analyically for the cylinder
    analytic_sfield = compute_sfield_exact(boundary, ifield, numerics);
    analytic_sfield = analytic_sfield .* (numerics.X.^2 + numerics.Y.^2 >= 1.5);

    % figure()
    % matlab_plot(boundary, analytic_sfield, numerics)
    % pause(1)

    % comptue the field numerical using integral equations
    phi = ie_solver(boundary, ifield, numerics);
    numeric_sfield = compute_sfield(boundary, ifield, phi, numerics);
    numeric_sfield = numeric_sfield .* (numerics.X.^2 + numerics.Y.^2 >= 1.5);

    % figure()
    % matlab_plot(boundary, numeric_sfield, numerics)
    % pause(1)

    % accuracy the accuracy as a function of mesh points
    error = abs(analytic_sfield - numeric_sfield);

    % figure()
    % pcolor(error)
    % shading interp
    % pause(1)

    % save in case of error in middle loops
    net_error = [net_error; mean(mean(error))]
    save('varError', 'n', 'net_error')
    % save('var_error', 'net_error')
end

loglog(n, net_error)
