function boundary = init_boundary(n_in)

    boundary.n = n_in;
    boundary.t_dim = 2*boundary.n;
    boundary.t_space = (pi / boundary.n) * (0:(boundary.t_dim-1)); 
    boundary.C = ones(1, boundary.t_dim);

    syms t
    boundary.funx(t) = cos(t);
    boundary.funy(t) = sin(t);

    boundary.X = double( boundary.funx(boundary.t_space) );
    boundary.Y = double( boundary.funy(boundary.t_space) );
    
    % length element
    diffx(t) = diff(boundary.funx, t);
    diffy(t) = diff(boundary.funy, t);

    boundary.ds = double(sqrt( diffx(boundary.t_space).^2 + ...
                               diffy(boundary.t_space).^2 ...
                               ));

end