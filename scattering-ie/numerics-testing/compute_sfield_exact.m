function sfield = compute_sfield_exact(boundary, ifield, numerics)

    k = norm(ifield.k);

    mag_XY = sqrt(numerics.X.^2 + numerics.Y.^2);

    num_terms = 100;
    sfield = zeros(numerics.ymesh_dim, numerics.xmesh_dim);
    thetas = theta(ifield.k, numerics.X, numerics.Y);
    for idx = -num_terms:1:num_terms

        sfield = sfield + ...
            ( -(i^idx) * (besselj(idx, k) / besselh(idx, 1, k)) ) * ...
            besselh(idx, 1, k*mag_XY) .* ...
            exp(i .* idx * thetas);
    end

    % term_mat = [];
    % for idx = 1:numerics.xmesh_dim
    %     term_mat(idx) = sum( A_n .* besselh(n, 1, k*mag_xy(idx)) .* ...
    %                        exp(i .* n * theta) ...
    %                        );
    % end
    
end
