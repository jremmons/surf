numerics = init_numerics();
boundary = init_boundary();

mesh_dim = 100;

t =  linspace(0, 2*pi, mesh_dim);
x_poly = double( boundary.funx(t) );
y_poly = double( boundary.funy(t) );

filename = 'tec/mask';
tecfile = fopen( [filename, '.tec'], 'w' );
fprintf( tecfile, ' title = "boundary mask" \n' );
fprintf( tecfile, ' variables = "x", "y", "mask"\n' );
fprintf( tecfile, ' zone n=%d, e=%d, datapacking = point, zonetype = fetriangle\n', ...
         mesh_dim+1, mesh_dim );

val = 0;

% the points
fprintf( tecfile, '%10.4f %10.4f %10.4f \n', ...
         0, 0, val );
for idx=1:mesh_dim
    fprintf( tecfile, '%10.4f %10.4f %10.4f \n', ...
             x_poly(idx), y_poly(idx), val );
end

% blank line
fprintf( tecfile, '\n');

% how the points connect
% the rest of the points
for idx=1:mesh_dim
    fprintf( tecfile, '%i %i %i\n', ...
             1, idx+1, idx+2);
end
