function plot_boundary(boundary)

    X = boundary.X;
    Y = boundary.Y;
    C = boundary.C;
    
    fill(X, Y, C);

end
