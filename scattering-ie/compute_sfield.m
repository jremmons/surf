function sfield = compute_sfield(boundary, ifield, phi, numerics)

    k = norm(ifield.k);

    sfield = zeros(numerics.xmesh_dim, numerics.xmesh_dim);    
    for t_idx=1:boundary.t_dim
        t = boundary.t_space(t_idx);
        x = double( boundary.funx(t) );
        y = double( boundary.funy(t) );

       sfield = sfield + besselh(0, 1, k * sqrt( (numerics.X - x).^2 + ...
                                                  (numerics.Y - y).^2 ) ...
                                  ) * ...
                 phi(t_idx) * ...
                 boundary.ds(t_idx);
    end
    sfield = (i*pi / (4*boundary.n)) * sfield;

end