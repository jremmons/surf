function boundary = init_boundary()

    boundary.n = 100;
    boundary.t_dim = 2*boundary.n;
    boundary.t_space = (pi / boundary.n) * (0:(boundary.t_dim-1)); 
    boundary.C = ones(1, boundary.t_dim);

    syms t
    % boundary.funx(t) = cos(t);
    % boundary.funy(t) = 2*sin(t);
    boundary.funx(t) = cos(t) + 0.65*cos(2*t) - 0.65;
    boundary.funy(t) = 1.5*sin(t);

    boundary.X = double( boundary.funx(boundary.t_space) );
    boundary.Y = double( boundary.funy(boundary.t_space) );
    
    % length element
    diffx(t) = diff(boundary.funx, t);
    diffy(t) = diff(boundary.funy, t);

    boundary.ds = double(sqrt( diffx(boundary.t_space).^2 + ...
                               diffy(boundary.t_space).^2 ...
                               ));

end