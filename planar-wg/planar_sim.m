%
% Author: John Emmons
% Description: a simpution of a wave propagating down a planar waveguide
%
clear all;

% define parameters
a_len = pi/3;
b_len = .6;
c_len = a_len - b_len;
w = 10;
c = 1;

z_len = 5; % +/- z_len on x-axis
k = w/c;

% Create video properties
writerObj = VideoWriter('movie.avi');
writerObj.FrameRate = 30;
writerObj.Quality = 75;
open(writerObj);

FigHandle = figure;
set(FigHandle, 'Position', [100, 100, 1.5*700, 1.5*512]);

% numerics parameters
NUM_MODES_A = 30;
NUM_MODES_B = 20;
NUM_MODES_C = 10;

MESH_POINTS_DIM_X = 100;
MESH_POINTS_DIM_Z = 500;

inc_coeffs = zeros(NUM_MODES_A,1);
inc_coeffs(2) = 1;
coeffs = compute_coeffs(a_len, b_len, c_len, k, ...
                        NUM_MODES_A, NUM_MODES_B, NUM_MODES_C, ...
                        inc_coeffs);

x_space = linspace(0, a_len, MESH_POINTS_DIM_X);
z_space = linspace(-z_len, z_len, MESH_POINTS_DIM_Z);

[Z,X] = meshgrid(z_space, x_space);
for t =0:.05:4*pi
    waveguide = zeros(MESH_POINTS_DIM_X, MESH_POINTS_DIM_Z);

    % draw region A (z < 0)
    for n=1:NUM_MODES_A      
        waveguide = waveguide + ...
            ( ...
                coeffs{1}(n) .* ...
                planar_mode(n, a_len, X, Z, -1, k) .* ... 
                exp(-i*w*t) ... 
                + ...
                coeffs{2}(n) .* ...
                planar_mode(n, a_len, X, Z, 1, k) .* ...
                exp(-i*w*t) ...
            ) .* (Z <= 0);
    end

    % draw region B (z > 0, x < b)
    for n=1:NUM_MODES_B
        waveguide = waveguide + ...
            coeffs{3}(n) .* ...
            planar_mode(n, b_len, X, Z, -1, k) .* ...
            exp(-i*w*t) .* ...
            (Z >= 0) .* ...
            (X <= b_len);
    end

    % draw region C (z > 0, x > b)
    for n=1:NUM_MODES_C
        waveguide = waveguide + ...
            coeffs{4}(n) .* ...
            planar_mode(n, c_len, (X-b_len), Z, -1, k) .* ...
            exp(-i*w*t) .* ...
            (Z >= 0) .* ...
            (X >= b_len);
    end

    pcolor(z_space, x_space, real(waveguide));
    line([0 z_len], [b_len b_len], 'Color', [0 0 0])
    shading interp
    colormap gray
    % caxis([-1, 1])
    pause(0.01)

    % Set some conditions for initial frame
    if t == 0
        set(gca,'nextplot','replacechildren');
        set(gcf,'Renderer','zbuffer');
    end
    frame = getframe(gcf);
    writeVideo(writerObj,frame);
end

close(writerObj);
