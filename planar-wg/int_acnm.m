function val = int_acnm(a_var, b_var, c_var, n_var, m_var)
    

    % syms a b c n m x
    % fun = sin(n*pi*x/a)*sin(m*pi*(x-b)/c);
    % fun = subs(fun, [a, b, c, n, m], [a_var, b_var, c_var, n_var, m_var]);    
    % val = double( int(fun, x, [b_var, b_var+c_var]) );

    % analytical expression
    val = (a_var*c_var*m_var*(a_var*sin((pi*b_var*n_var)/a_var) - a_var*sin((pi*n_var*(b_var + c_var))/a_var)*cos(pi*m_var)) + ...
           a_var*c_var^2*n_var*cos((pi*n_var*(b_var + c_var))/a_var)*sin(pi*m_var))/(pi*(a_var^2*m_var^2 - c_var^2*n_var^2));

end