function val = int_abnn(b_var, n_var)

    % syms b n x
    % fun = sin(n*pi*x/b)^2;
    % fun = subs(fun, [b, n], [b_var, n_var]);    
    % val = double( int(fun, x, [0, b_var]) );

    % analytical expression
    val = b_var/2 - (b_var*sin(2*pi*n_var))/(4*pi*n_var);

end