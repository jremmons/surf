function val = int_abnm(a_var, b_var, n_var, m_var)

    % syms a b n m x
    % fun = sin(n*pi*x/a) * sin(m*pi*x/b);
    % fun = subs(fun, [a, b, n, m], [a_var, b_var, n_var, m_var]);
    % val = double( int(fun, x, [0, b_var]) );

    % analytical expression
    val = -(a_var*b_var*(a_var*m_var*cos(pi*m_var)*sin((pi*b_var*n_var)/a_var) - b_var*n_var*sin(pi*m_var)*cos((pi*b_var*n_var)/a_var)))/(pi*(a_var^2*m_var^2 - b_var^2*n_var^2));

end