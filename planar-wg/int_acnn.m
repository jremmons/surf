function val = int_acnn(b_var, c_var, m_var)
    
    % syms b c m x
    % fun = sin(m*pi*(x-b)/c)^2;
    % fun = subs(fun, [b, c, m], [b_var, c_var, m_var]); 
    % val = double( int(fun, x, [b_var, b_var+c_var]) );

    val = c_var/2 - (c_var*sin(2*pi*m_var))/(4*pi*m_var);

end