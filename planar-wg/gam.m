function g = gam(n, d, k)
    
    if n*pi/d < k % propagating wave
        g = -sqrt( (n*pi/d)^2 - k^2 );
    else % exp decaying wave
        g = sqrt( (n*pi/d)^2 - k^2 );
    end

end