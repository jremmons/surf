function amp = planar_mode(n, d, x, z, sign, k)

    g = gam(n, d, k);

    amp = sin(n*pi*x/d) .* exp( sign * g .* z);

end 