function [coeffs] = compute_coeffs(a_len, b_len, c_len, k, ...
                                   NUM_MODES_A, NUM_MODES_B, NUM_MODES_C, ...
                                   incident)

    % create the matrix system
    N = NUM_MODES_A + NUM_MODES_B + NUM_MODES_C;

    A = zeros(N);
    B = zeros(N,1);

    % continuity along A-B
    for i_idx=1:NUM_MODES_B
        eq_offset = 0;        
        for j_idx=1:NUM_MODES_A 
            A(i_idx + eq_offset, j_idx) = int_abnm(a_len, b_len, j_idx, i_idx);
        end
        
        var_offset = NUM_MODES_A;
        A(i_idx + eq_offset, i_idx + var_offset) = -int_abnn(b_len, i_idx);

        sum = 0;
        for j_idx=1:NUM_MODES_A
            sum = sum + incident(j_idx) * int_abnm(a_len, b_len, j_idx, i_idx);
        end
        B(i_idx + eq_offset) = -sum;

    end

    % continuity of derivative along A-B
    for i_idx=1:NUM_MODES_B
        eq_offset = NUM_MODES_B;
        for j_idx=1:NUM_MODES_A 
            A(i_idx + eq_offset, j_idx) = gam(j_idx, a_len, k) * ...
            int_abnm(a_len, b_len, j_idx, i_idx);
        end
        
        var_offset = NUM_MODES_A;
        A(i_idx+eq_offset, i_idx+var_offset) = gam(i_idx, b_len, k) * ...
            int_abnn(b_len, i_idx);

        sum = 0;
        for j_idx=1:NUM_MODES_A
            sum = sum + incident(j_idx) * gam(j_idx, a_len, k) * ...
                  int_abnm(a_len, b_len, j_idx, i_idx);
        end
        B(i_idx + eq_offset) = sum;
        
    end
    
    % continuity along A-C
    for i_idx=1:NUM_MODES_C
        eq_offset = 2*NUM_MODES_B;        
        for j_idx=1:NUM_MODES_A 
            A(i_idx + eq_offset, j_idx) = int_acnm(a_len, b_len, c_len, ...
                                                   j_idx, i_idx);
        end
        
        var_offset = NUM_MODES_A + NUM_MODES_B;
        A(i_idx + eq_offset, i_idx + var_offset) = -int_acnn(b_len, c_len, ...
                                                          i_idx);

        sum = 0;
        for j_idx=1:NUM_MODES_A 
            sum = sum + incident(j_idx) * int_acnm(a_len, b_len, c_len, ...
                                                   j_idx, i_idx);
        end
        B(i_idx + eq_offset) = -sum;

    end

    % continuity of derivative along A-C
    for i_idx=1:NUM_MODES_C
        eq_offset = 2*NUM_MODES_B + NUM_MODES_C;        
        for j_idx=1:NUM_MODES_A 
            A(i_idx + eq_offset, j_idx) = gam(j_idx, a_len, k) * ... 
                int_acnm(a_len, b_len, c_len, j_idx, i_idx);
        end
        
        var_offset = NUM_MODES_A + NUM_MODES_B;
        A(i_idx+eq_offset, i_idx+var_offset) = gam(i_idx, c_len, k) * ...
            int_acnn(b_len, c_len, i_idx);

        sum = 0;
        for j_idx=1:NUM_MODES_A 
            sum = sum + incident(j_idx) * gam(j_idx, a_len, k) * ...
            int_acnm(a_len, b_len, c_len, j_idx, i_idx);
        end
        B(i_idx + eq_offset) = sum;

    end

    % solve the matrix system
    X_coeffs = linsolve(A, B);
   
    % pack the answer
    coeffs = cell(4,1);
    coeffs{1} = incident; 
    coeffs{2} = X_coeffs(1 : NUM_MODES_A);
    coeffs{3} = X_coeffs(NUM_MODES_A+1 : NUM_MODES_A+NUM_MODES_B);
    coeffs{4} = X_coeffs(NUM_MODES_A+NUM_MODES_B+1 : ...
                         NUM_MODES_A+NUM_MODES_B+NUM_MODES_C);

end