% Created by tikzDevice version 0.8.1 on 2015-08-17 09:31:47
% !TEX encoding = UTF-8 Unicode
\documentclass[12pt,authoryear]{elsarticle}
\nonstopmode

\usepackage{tikz}

\usepackage[active,tightpage,psfixbb]{preview}

\PreviewEnvironment{pgfpicture}

\setlength\PreviewBorder{0pt}

\newcommand{\SweaveOpts}[1]{}  % do not interfere with LaTeX
\newcommand{\SweaveInput}[1]{} % because they are not real TeX commands
\newcommand{\Sexpr}[1]{}       % will only be parsed by R


\makeatletter % remove preprint footer
\def\ps@pprintTitle{%
 \let\@oddhead\@empty
 \let\@evenhead\@empty
 \def\@oddfoot{}%
 \let\@evenfoot\@oddfoot}
\makeatother

% \documentclass[preprint,12pt,authoryear]{elsarticle}

% R CONFIGURATION
% set R global options in opts_chunk$set(...)
%
% dev   : what output device to use (tex, png, pdf, etc) 
% echo  : output code used to generate the figure
% cache : only update figure if the embedded R code changed
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tex preamble
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[activate={true,nocompatibility},final,tracking=true,kerning=true,
  spacing=true,factor=100,stretch=25,shrink=25]{microtype}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{ulem}

\usepackage{lipsum}

\newcommand{\vect}[1]{\boldsymbol{\mathbf{#1}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{tikzpicture}[x=1pt,y=1pt]
\definecolor{fillColor}{RGB}{255,255,255}
\path[use as bounding box,fill=fillColor,fill opacity=0.00] (0,0) rectangle (505.89,505.89);
\begin{scope}
\path[clip] (100.37,124.85) rectangle (454.48,405.52);
\definecolor{drawColor}{RGB}{0,0,255}

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (113.48,389.92) --
	(157.86,379.36) --
	(199.78,371.55) --
	(240.79,361.43) --
	(280.51,352.31) --
	(323.29,342.43) --
	(365.50,332.41) --
	(407.67,324.97);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (113.48,389.92) circle (  4.59);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (157.86,379.36) circle (  4.59);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (199.78,371.55) circle (  4.59);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (240.79,361.43) circle (  4.59);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (280.51,352.31) circle (  4.59);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (323.29,342.43) circle (  4.59);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (365.50,332.41) circle (  4.59);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (407.67,324.97) circle (  4.59);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,505.89);
\definecolor{drawColor}{RGB}{0,0,0}

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (113.48,124.85) -- (441.37,124.85);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (113.48,124.85) -- (113.48,112.61);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (189.35,124.85) -- (189.35,112.61);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (289.64,124.85) -- (289.64,112.61);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (365.50,124.85) -- (365.50,112.61);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (441.37,124.85) -- (441.37,112.61);

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at (113.48, 80.78) {10};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at (189.35, 80.78) {20};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at (289.64, 80.78) {50};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at (365.50, 80.78) {100};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at (441.37, 80.78) {200};

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,135.24) -- (100.37,366.25);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,135.24) -- ( 88.13,135.24);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,193.00) -- ( 88.13,193.00);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,250.75) -- ( 88.13,250.75);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,308.50) -- ( 88.13,308.50);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,366.25) -- ( 88.13,366.25);

\node[text=drawColor,rotate= 90.00,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at ( 70.99,135.24) {1e-10};

\node[text=drawColor,rotate= 90.00,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at ( 70.99,250.75) {1e-06};

\node[text=drawColor,rotate= 90.00,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at ( 70.99,366.25) {1e-02};

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,124.85) --
	(454.48,124.85) --
	(454.48,405.52) --
	(100.37,405.52) --
	(100.37,124.85);
\end{scope}
\begin{scope}
\path[clip] (  0.00,  0.00) rectangle (505.89,505.89);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  2.04] at (277.43,447.26) {\bfseries 2D Integration on Flower};

\node[text=drawColor,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at (277.43, 31.82) {Width of X \& Y grid (constant point density)};

\node[text=drawColor,rotate= 90.00,anchor=base,inner sep=0pt, outer sep=0pt, scale=  1.70] at ( 22.03,265.18) {Absolute error};
\end{scope}
\begin{scope}
\path[clip] (100.37,124.85) rectangle (454.48,405.52);
\definecolor{drawColor}{RGB}{255,0,0}

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (113.48,399.10) --
	(157.86,384.09) --
	(199.78,364.33) --
	(240.79,339.19) --
	(280.51,309.04) --
	(323.29,269.22) --
	(365.50,220.08) --
	(407.67,159.26);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (113.48,406.23) --
	(119.67,395.53) --
	(107.30,395.53) --
	(113.48,406.23);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (157.86,391.23) --
	(164.04,380.52) --
	(151.68,380.52) --
	(157.86,391.23);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (199.78,371.47) --
	(205.96,360.76) --
	(193.60,360.76) --
	(199.78,371.47);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (240.79,346.33) --
	(246.97,335.63) --
	(234.61,335.63) --
	(240.79,346.33);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (280.51,316.18) --
	(286.69,305.47) --
	(274.33,305.47) --
	(280.51,316.18);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (323.29,276.36) --
	(329.47,265.65) --
	(317.11,265.65) --
	(323.29,276.36);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (365.50,227.22) --
	(371.68,216.51) --
	(359.32,216.51) --
	(365.50,227.22);

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (407.67,166.40) --
	(413.85,155.69) --
	(401.49,155.69) --
	(407.67,166.40);
\definecolor{drawColor}{RGB}{0,0,0}

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (100.37,198.29) rectangle (319.33,124.85);
\definecolor{drawColor}{RGB}{255,0,0}

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (118.73,180.95) --
	(124.91,170.24) --
	(112.55,170.24) --
	(118.73,180.95);
\definecolor{drawColor}{RGB}{0,0,255}

\path[draw=drawColor,line width= 0.4pt,line join=round,line cap=round] (118.73,149.33) circle (  4.59);
\definecolor{drawColor}{RGB}{0,0,0}

\node[text=drawColor,anchor=base west,inner sep=0pt, outer sep=0pt, scale=  1.70] at (137.09,166.78) {With windowing};

\node[text=drawColor,anchor=base west,inner sep=0pt, outer sep=0pt, scale=  1.70] at (137.09,142.30) {Without windowing};
\end{scope}
\end{tikzpicture}

\end{document}
