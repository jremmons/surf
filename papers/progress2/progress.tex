\documentclass[12pt,authoryear]{elsarticle}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\makeatletter % remove preprint footer
\def\ps@pprintTitle{%
 \let\@oddhead\@empty
 \let\@evenhead\@empty
 \def\@oddfoot{}%
 \let\@evenfoot\@oddfoot}
\makeatother

% \documentclass[preprint,12pt,authoryear]{elsarticle}

% R CONFIGURATION
% set R global options in opts_chunk$set(...)
%
% dev   : what output device to use (tex, png, pdf, etc) 
% echo  : output code used to generate the figure
% cache : only update figure if the embedded R code changed
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tex preamble
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[activate={true,nocompatibility},final,tracking=true,kerning=true,
  spacing=true,factor=100,stretch=25,shrink=25]{microtype}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{ulem}

\usepackage{lipsum}

\newcommand{\vect}[1]{\boldsymbol{\mathbf{#1}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}
\begin{frontmatter}

%% Title, authors and addresses

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for theassociated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for theassociated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for theassociated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}

\title{A High-Order Method for Wave Propagation in 3D Dielectric Waveguides of Arbitrary Transverse Shape\\
  {\small Progress as of \today}
}
\journal{Computational Physics}

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{}
%% \address[label1]{}
%% \address[label2]{}

\author{John R. Emmons, Emmanuel Garza, and Oscar Bruno}
\address{California Institute of Technology, Applied and Computational Mathematics\\
1200 East California Boulevard, Pasadena, CA 91125}

\begin{abstract}
Dielectric waveguides play essential roles in fiber optic devices, wireless
communication, and other specialized electronics. However, modelling the
propagation of electromagnetic fields in dielectric waveguides of arbitrary
shape presents several challenges. In particular, the mode spectrum consists of
a discrete and continuous set of eigenfunctions and the continuous modes
requires special treatment to avoid low order convergence. Previous works do not
fully address this challenge or ignore it altogether, limiting their use to
simplified cases and making them unsuitable for physically realistic
simulations.

We present progress on a general, high-order numerical method for simulating
dielectric waveguides with complex 3D geometries requiring only modest
computational resources (a single workstation). The method is based on first
expanding fields into the modes of the waveguide, then propagating each mode
independently. We characterize the continuous spectrum of radiation modes and
make progress toward a general prescription for its discretization. Using this
approach, we can compute a highly accurate expansion of an arbitrary field into
the modes of the waveguide, including discrete and continuous modes, yielding a
more general technique for high precision dielectric waveguide simulations.
\end{abstract}
\end{frontmatter}

\section{Introduction}\vspace{-.25cm}

Metallic and dielectric waveguides (e.g. optical fibers) direct electromagnetic
waves to travel along their optical axis. The propagation of electromagnetic
waves inside a waveguide is often described approximately using a geometric
optics approximation in which only the center of energy of the wave is
tracked. However, waveguides with diameters near the wavelength of a propagating
signal cannot be accurately described by this approximate geometric model;
rather, their electromagnetic properties (e.g. optical modes) must be analyzed
by finding solutions to Maxwell's equations.

In most cases, it is impossible to find closed-form solutions for waveguides
with complex boundaries/shapes. Hence, numerical methods must be used to
approximate the solutions of these problems. Developing accurate numerical
methods for simulating the propagation of electromagnetic fields inside and
outside of a dielectric waveguide is essential for continued advancement in the
design of specialized devices such as dielectric antennas with complicated
geometries.

Previous work in this area has focused only on circular waveguides
\cite{Snyder1969}, where solutions have a closed form expression. Even for the
circular case, it is typical to do certain approximations (e.g. assuming that
there is a small dielectric difference between the core and cladding, which
leads to ignoring the reflected field at interfaces). For the case of 2D
waveguides, a more complete set of theory and methods exists since there exist
an analytic expression for the modes of this type of planar waveguides
\cite{magnanini}, but the generalization to three dimensions is not
trivial. Modes for 3D waveguides depend on the geometry and there only exist
closed form expressions for highly symmetric waveguides.

Even more recent efforts still focus on radially symmetric
waveguides \cite{Alexandrov2003}. On the other hand, little attention has been
given to arbitrary shaped 3D waveguides beyond computing the field as an
expansion of bound modes \cite{Lu2012}; as a result, radiation away from the
core of the waveguide cannot be modelled. Other methods that take into account
the fields away from the core usually suffer from different
limitations. Previous works introduce a boundary in order to truncate the domain
\cite{Ciraolo} which leads to artificial reflections that do not correspond to
the real physical behavior of the fields.

The goal of this project is to develop and characterize a robust numerical
method to analyze the excitation of optical modes (bounded and unbounded) in a
dielectric waveguide with an arbitrary incident field. We will use a physically
realistic model which unlike previous works
\cite{Ciraolo,Lu2012,Alexandrov2003}, does not introduce approximations to the
domain or limit use to symmetric systems. This method will expand the field into
eigenmodes inside the waveguide and use integral equation methods for the fields
in the surrounding exterior region.

% \subsection{Physical Description}
% knit_child('sections/physical_description.Rnw')

\section{Challenges}

This project presents several challenges: due to the open nature of dielectric
waveguides (i.e. the fields extend to infinity), the complete set of modes
include a continuous spectrum that makes the mode decomposition to be a mix of
an integral with unknown amplitude function, in addition to the tradition sum of
modes with unknown amplitude coefficients.

However, the main challenge in the simulation of such problems is the treatment
of the discontinuous permittivity, since it reduces the spatial order of
accuracy to first order (even for high-order methods) if no special
considerations are made. The aim of the project is to find ways to overcome this
difficulty.

\section{Objectives}

In this project, we seek to develop an accurate methodology for solving the
problem of illumination of a dielectric waveguide of arbitrary transverse shape
by an arbitrary incident electromagnetic field (figure
\ref{fig:illumination}). This work extends current efforts by Professor's Bruno
group related to mode expansions of dielectric waveguides of arbitrary
geometries. Currently, an efficient way to find the modal fields exists, however
it is still uncertain as to what is the best numerical approach in order to use
these modal fields for solving the problem of arbitrary illumination, since it
is not clear what is the best way to discretize the continuous spectrum.

\begin{figure}[t]\centering
  \includegraphics[scale=0.5]{images/illumination_waveguide/illumination}
  \caption{Graphical depiction of the illumination problem. Given a known
    incident field that illuminates the waveguide, we seek to find the reflected
    and transmitted fields.}\label{fig:illumination}
\end{figure}

The problem of illumination of an arbitrary shape waveguide with known modal
functions carries out two main difficulties: the first is to compute accurately
the modal amplitudes (coefficients for the discrete spectrum and functions for
the continuous part of the spectrum); and the second challenge is to account for
the reflected field. Our objective is to solve both these challenges by the end
of the summer.

\section{Progress: (as of \today)}

In the time since the start of my SURF and the first progress report, I
primarily worked on smaller projects which helped me master the fundamentals of
numerical PDEs. This included solving Maxwell's equations for a PEC 3D
waveguide, implementing mode matching for a 2D planar waveguide, and using
integral equations to solve the 2D scattering problem for an arbitrary shaped
object with Dirichlet boundary conditions. Since then, I have stopped working on
practice problem and have started to contribute to the group in a substantive
ways.

\subsection{Projections for 3D Dielectric Waveguide of Arbitrary Shape}

The solutions to Maxwell's equations in a 3D dielectric waveguide has a high
degree of degeneracy. In fact, in the case of a dielectric waveguide, there are
uncountably many solutions which satisfy the governing PDEs. We seek to compute
the expansion of an arbitrary field into these solutions (i.e. modes) of the
waveguide so we can simulate the propagation of the field. This is analogous to
finding the Fourier transform for a one-dimensional function. In the same way,
we can recover the original function by multiplying the modes by their
corresponding coefficients and integrating.

\begin{subequations}
  \begin{align}
  \vect{E}_{total} &= \sum_i a_i\; \vect{E}^i_{bound} + \sum_i \int_0^\infty a_i(Q)\; \vect{e}_i\; e^{i\beta(Q)z}\; dQ\\
  \intertext{where}
  a_i &= \frac{1}{2N_i}\int_{A_\infty}\vect{E}_t \times \vect{h}_{ti}^* \cdot \vect{\hat{z}}\;dA\label{eqn:coeff}
  \end{align}
\end{subequations}

Note that $\vect{E}_{total}$ (the total electric field) is composed of a
discrete (the first sum) and a continuous (the integral) spectrum of mode; the
same is true of the magnetic field $\vect{H}_{total}$. Choosing an appropriate
discretization of the continuous modes is one of the primary research questions
I am trying to answer this summer.

In order to compute the projection of an arbitrary field onto the modes of the
waveguide (i.e. the coefficient in \ref{eqn:coeff}), we need to be able to
compute 2-dimensional integrals with very high accuracy. This is a non-trivial
task in our case since the field is discontinuous at the boundary of the
interior and exterior of the waveguide. As a result, we cannot use simple
methods (such as trapezoidal integration) to preform these integrals because
they are low order when there are discontinuities in the function.

\begin{figure*}[t!]\centering
  
  \begin{tabular}{cc}

    \begin{subfigure}[b]{0.5\columnwidth}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\columnwidth]{figure/gridPP-1} 

\end{knitrout}

    \end{subfigure}

    &

    \begin{subfigure}[b]{0.5\columnwidth}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\columnwidth]{figure/gridFF-1} 

\end{knitrout}

    \end{subfigure}
  \end{tabular}

  \caption{These meshes are generated by the adaptive grid implementation using
    different strategies to assign points to subintervals. Each mesh uses the
    same number of points, but arranges them differently. [Left] In the scaled
    strategy, points are assigned evenly along each dimension. In this way,
    intervals receive a number of mesh points proportional to their length (like
    the House of Representatives in Congress). [Right] In the even strategy,
    points are split evenly among intervals without regard for their relative
    length (like the Senate in Congress). When tested, the even distribution
    strategy produces the highest order of
    convergence (proof shown in previous progress report).} \label{fig:adaptive-grids}
\end{figure*}

To address this issue, I helped design and implement a generic 2D integration
method which is high order regardless of the shape of the waveguide boundary.
The idea is to divide the domain of integration in such as way as to avoid
integrating over any discontinuities, then use high order methods for each
smaller intervals.

\begin{figure}[t!]

  \begin{tabular}{cc}
    \begin{subfigure}{0.5\columnwidth}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\columnwidth]{figure/nowin-error-1} 

\end{knitrout}
    \end{subfigure}
    
    &

    \begin{subfigure}{0.5\columnwidth}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\columnwidth]{figure/flower-win-v-nowin-error-1} 

\end{knitrout}
    \end{subfigure}
  \end{tabular} 

  \caption{We integrate a modified Hankel function ($\frac{1}{z^2}H^1_0(z)$) on
    $\mathbb{R}^2$ outside of a cylindrical and flower shaped waveguide. [Left]
    For the cylindrical waveguide, the Hankel function is centered at the
    origin. The error of integration is found by comparing the numerical result
    to the analytical solution. [Right] For the flower waveguide, the Hankel
    function is centered at $(0.5, 0.5)$ (a point still inside the waveguide but
    no longer symmetric). The error is computed by comparing the numerical
    result to a value obtained by using same method with a much larger window
    size (assumed to be converged). In both cases, as the truncated domain of
    integration is increased, the convergence is exponential with the windowing
    function and is approximately second order without the windowing
    function.}\label{fig:win-conv}
\end{figure}

To divide the region, I first identify locations along the curve where the x
derivative of the parameterization is zero (i.e where the curve goes straight up
or down in figure \ref{fig:adaptive-grids}). These points represents locations
along the x-dimension where the function could have a discontinuous jump. I then
discretize these intervals using a high order method (Colton \& Kress) to obtain
the location of the columns (i.e. x grid). The next step is to identify where
the columns intersect the curve and create grids in a similar way as before in
the y dimension (creating the y grid). Combining these two, we have a
non-uniform grid in x and y which avoids discontinuities. This adaptive grid is
able to achieve high order accuracy even for functions with $C^1$
discontinuities.

\subsection*{Convergence of Expansion Coefficients (2d Integration Method)}

Once the adaptive grid implementation was completed and fully debugged, the next
step was to test the convergence of the method for a realistic function. In
general, the continuous modes oscillate and decay slowly (like
$\frac{1}{\sqrt{r}}$), making the convergence of na\"{i}ve integration methods
slow. Previous work done in this area by Professor Bruno, et. el. confirms that
using simple integration methods such as trapezoidal integration are low order
when the domain of integration is infinite (e.g. $\mathbb{R}^2$ in our
case). The low order of convergence is the result of truncating the domain and
making the following approximation.

$$ \int_{-\infty}^{\infty} \frac{e^{ir}}{\sqrt{r}} dr \approx
\int_{-A}^{A} \frac{e^{ir}}{\sqrt{r}} dr \text{~~~(for large A, but converges slowly)}$$

The solution is to apply the Bruno-Monro windowing function to the integrand. A
discussion of this function is beyond the scope of this progress report, but by
modifying the integrand, super-algebraic convergence can be achieved
\cite{monro}. See figure \ref{fig:win-conv}, for an illustration of the windowing
function compared to simple truncation. With the addition of the windowing
function, the integration method developed during the first progress report is
ready to used for computing eigenfunction expansion coefficients.

%% \begin{figure}[t]
%%   <<win-error, out.width='\\columnwidth', message=F, warning=F>>=
%%   library('R.matlab')
%%   wdata100 <- readMat('../data/winConv100.mat')
%%   wdata500 <- readMat('../data/winConv500.mat')

%%   width100 <- unlist(wdata100[1])
%%   error100 <- unlist(wdata100[2])

%%   width500 <- unlist(wdata500[1])
%%   error500 <- unlist(wdata500[2])

%%   plot(width100, error100, type = "o", col = "blue", log = "xy", 
%%   main = "2d Windowing Function Convergence", 
%%   xlab="Width of X \\& Y grid (constant point density)", 
%%   ylab="Absolute error", ylim=c(1e-11, 1e-1))

%%   lines(width500, error500, type = "o", col = "red", pch = 2)

%%   legend("topright", 
%%   c("10 points per unit length", "50 points per unit length"), 
%%   col=c("blue", "red"), pch=c(1,2))

%%   @
%%   \caption{The convergence of the 2d windowing function while integrating the
%%     Hankel function ($\frac{1}{z^2}H^1_0(z)$) from $r = 1$ to infinity (i.e. outside 
%%     the unit circle). The curve with 10 points per unit length is unable to 
%%     reach machine precision because the error is bounded below by the mesh 
%%     point density, so increasing the width of the integration does not improve
%%     accuracy. On the other hand, the curve with 50 points per unit length is 
%%     able to continue converging because the error is not bounded by the mesh
%%     point density.}
%% \end{figure}

\subsection*{Convergence of Modal Eigenvalues}

\begin{figure}[t!]\centering
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=0.5\columnwidth]{figure/kz-convergnece-error-1} 

\end{knitrout}
  \caption{The convergence of the discrete mode eigenvalues for the flower are
    shown as the the number of points used to discretize the six single layer
    densities along boundaries of the waveguide is increased. This illustrates
    that the methods used to compute these eigenvalues has high order
    convergence and is suitable for our waveguide simulation
    method.}\label{fig:eigen-conv}
\end{figure}

After completing the integration method for computing the expansion
coefficients, it was now possible to propagate an arbitrary field along the
waveguide with high accuracy. However, before additional work can be done, it
was important to comfirm the expected order of convergence is acheived with all
the parts in place. Debugging at this stage was especially important because the
problems we are solving are sufficiently complicated that we cannot confirm with
analytical results. One check I performed was to ensure that the computed
eigenvalues for the discrete and continuous spectrum converged appropriately.
asdfasdf

In figure \ref{fig:eigen-conv}, I show the convergence of the six discrete
eigenvalues for the flower geometry. We used a reliable benchmark as the
accepted value to compute the error. As expected, the convergence is high
order (approximately sixth order) and is same for all eigenvalues.

\begin{figure}[t!]\centering
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=0.65\columnwidth]{figure/continuous-mode-convergnece-1} 

\end{knitrout}
  \caption{A continuous mode with frequency ($k_e$) of $0.9479$ is projected
    onto other continuous modes to test orthogonality. The different lines show
    the orthogonality obtained for different sized domains of integration. As
    the domains tend to infinity, the curves approach a Dirac delta function
    demonstrating that as our discretization grows larger we approach the
    expected analytical result.} \label{fig:con-modes}
\end{figure}

\subsection*{Orthogonality of Discrete and Continuous Modes}

As part of confirming the correctness of the codes, we also performed tests to
ensure the discrete and continuous modes we computed numerical were orthogonal,
meaning the inner product of any mode (discrete or continuous) with any other
mode was zero. In the case of discrete modes, we saw that the spectrum was
indeed orthogonal up to the accuracy of the methods used.

In the case of continuous modes, confirming orthogonality was less trivial
because continuous modes are not integrable. In the analytical case, we would
expect the inner product of a continuous modes with other continuous modes to be
a Dirac delta function centered at the frequency of the mode. Since we
discretized (and truncate) the spatial dimension used to compute inner products,
it was impossible to get the analytical result numerically. However, we can
still confirm orthogonality by examining the limiting behavior of the inner
product. Figure \ref{fig:con-modes}, illustrates the orthogonality of the modes
as the mesh used to compute the inner product tends to $\mathbb{R}^2$. As expected,
the curve approaches a delta function.

%% \subsection*{Propagating of an Arbitrary Field in 3d}
%% If there in time before the progress report is due.

%\section{Method}
%knit_child('sections/method.Rnw')

%\section{Algorithm/Implementation}
%knit_child('sections/algorithm.Rnw')

\section{Work Plan}


In the remaining weeks of the SURF, I will continue to implement more features
for the 3D waveguide method. However, my focus will be on producing meaningful
plots and figures for a possible publication since we have already completed a
significant amount of work.

\begin{itemize}
\item \sout{ Week 1: Introduction - Solve model problems for numerical integration,
  mode-matching problems and integral equations. }
\item \sout{ Week 2-3: Solve the illumination problem for the 2D dielectric planar
  waveguide case. }
\item \sout{ Weeks 4-5: Implement the 2D numerical integration method based on the
  windowing function in order to compute the modal amplitudes. }
\item \sout{Weeks 6-7: Solve the 3D illumination problem by ignoring the reflected
  field (approximate the transmitted field by the incident field).}
\item Weeks 7-10: Implement a mode-matching scheme in order to account for the
  reflected fields.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\nocite{*}
%\bibliographystyle{plain}
\bibliographystyle{abbrv}
\bibliography{progress} 

\end{document}
