
\subsection{Mode Matching for PEC in 2D \& 3D}

In the first week, I implemented a code to take an arbitrary expansion of modes
(i.e. an EM wave) in a perfect electrical conductor (PEC) planar waveguide and
propagate this expansion (wave) past a discontinuity. In this particular
exercise, the waveguide (width $a$) was split into two smaller waveguides (width
$b$ and $c$ such that $a = b + c$) by a perfect barrier. In order to do this, I
first solved (analytically) for the modes of a rectangular PEC waveguide in 2
and 3 dimensions. The solution for this was already known and can be readily
found in many introductory electromagnetic theory text books. However, deriving
the expressions starting with only Maxwell's Equations was a good exercise for
reviewing the basics.

Once I had derived the expressions for the modes, I implemented a simple code in
MATLAB to propagate and visualize the fields in time. The final step was to
solve for the mode coefficients in the waveguides of width $b$ and $c$ given the
coefficients in the waveguide of width $a$. This was done by solving a linear
system of equations after applying the mode matching procedure.

\subsection{Integral Equation Methods for 2D Scattering}

\begin{figure}[t]\centering
  
  \begin{tabular}{cc}
    \begin{subfigure}[b]{0.5\columnwidth}
      \includegraphics[height=6.25cm]{images/integral_equations/rocket}
    \end{subfigure}

    &

    \begin{subfigure}[b]{0.5\columnwidth}
      <<ie-error, out.width='\\columnwidth', message=F, warning=F>>= 
      library('R.matlab')
      data <- readMat('../data/ieError.mat')

      par( cex = 1.7 )

      pts <- unlist(data[1])
      error <- unlist(data[2])

      plot(pts, error, type = "o", log = "xy", 
      main = "Integral Equation Convergence \n For Scattering (Cylinder)", 
      xlab="Discretization points along surface boundary", 
      ylab="Average error in grid points around object")
      @
    \end{subfigure}
  \end{tabular}
  
  \caption{[left] A field coming from the $+x$ axis hits a 2D PEC kite and
    scatters; [right] the integral equation methods used to produce this simulation 
    converge spectrally to machine precision (double precision).}\label{fig:rocket}
\end{figure}

An integral equation is an equation where the function of interest appears 
under an integral sign. For example,

\begin{equation}
  f(x) = \int K(x,t)\,\varphi(t)\,dt. 
\end{equation}

\noindent
is an integral equation if $f(x)$ and $\varphi(t)$ and known and the function of
interest is $K(x,t)$. To solve these problems numerically, we discretize the
variable $x$, then solve the linear systems of equations that is created by
approximating the integral.

These methods have many advantages over, simpler methods; for example, finite
difference methods must spatially discretize a domain; however, this is
nontrivial for problems where the domain is infinite. In the case of scattering,
the fields extend to infinity everywhere, so special consideration would have to
be taken to solve these problems with finite differences. With integral
equations though, we do not discretize the domain spatially; rather, we
discretize along the boundary of the scattering object. No special consideration
is required to account for truncatation or fields which do not decay rapidly.

For scattering, the unknown function is the electron density along the boundary
of the PEC object. It is easy to compute the field everywhere once the density
along the boundary is known, so my goal was to write a code to find this density
using integral equations. Figure \ref{fig:rocket} (on the left), shows the
results of my work for a kite shaped boundary. The field is plotted around the
object in black with an incident field from the $+x$ axis.

On the right of figure \ref{fig:rocket}, I show the convergence of the integral
equation methods for the scattering problem for a cylinder (for which the
analytical solution is easy to compute). Note that the error between the
numerical and analytical solution decreases super-linearly as the number of mesh
points along the boundary increases. 

By implementing an integral equations method for the 2D scattering problem, I
learned how to apply integral equations to more complex problems. The next step
will be to use integral equations to compute the densities in a 3D and
dielectric domain.

\subsection{Projections for 3D Dielectric Waveguide of Arbitrary Shape}

In order to compute the projection of an arbitrary field onto the modes of the 
waveguide, we need to be able to compute 2-dimensional integrals with very high
order accuracy quickly. This is a non-trivial task in our case since the field
is discontinuous at the boundary of the interior and exterior of the waveguide.
As a result, we cannot use simple methods (such as the trapezoid method) to 
preform these integrals because they produce low order accuracy when there are
discontinuities in the function. 

To address this issue, I helped to design and implement a generic 2d integration
method which is high order regardless of the shape of the waveguide boundary.
The main idea is to divide the region of integration in such as way as to avoid
integrating over any discontinuities, then use high order methods for each
smaller interval. To divide the region, I first identify locations along the
curve where the x derivative of the parameterization is zero (i.e where the
curve goes straight up or down in figure \ref{fig:adaptive-grids}). These points
represents locations along the x-dimension where the function could have a
discontinuous jump. I then discretize these intervals using a high order method
to obtain the location of the columns (i.e. x grid). The next step is to
identify where the columns intersect the curve and create grids in a similar way
as before in the y dimension (creating the y grid). Combining these two, we have
a non-uniform grid in x and y which avoids discontinuities and achieves high
order accuracy.

\begin{figure*}[t]\centering
  
  \begin{tabular}{cc}

    \begin{subfigure}[b]{0.5\columnwidth}
      <<gridPP, out.width='\\columnwidth', message=F, warning=F>>= 
      library('R.matlab')
      grid <- readMat('../data/flowerGridPP-v6.mat')
      par( cex = 1.7 )

      xGrid <- unlist(grid[1])
      yGrid <- unlist(grid[2])

      xCurve <- unlist(grid[3])
      yCurve <- unlist(grid[4])

      plot(xGrid, yGrid, pch = 20, cex = 0.35, 
      main = "X and Y Scaled Grid", 
      xlab="X grid dimension", 
      ylab="Y grid dimension", col = "red")
      
      lines(xCurve, yCurve, type = "l", col = "black", lwd = 5)
      @

    \end{subfigure}

    &

    \begin{subfigure}[b]{0.5\columnwidth}
      <<gridFF, out.width='\\columnwidth', message=F, warning=F>>= 
      library('R.matlab')
      grid <- readMat('../data/flowerGridFF-v6.mat')
      par( cex = 1.7 )

      xGrid <- unlist(grid[1])
      yGrid <- unlist(grid[2])

      xCurve <- unlist(grid[3])
      yCurve <- unlist(grid[4])

      plot(xGrid, yGrid, pch = 20, cex = 0.35, 
      main = "X and Y Evenly Distributed Grid", 
      xlab="X grid dimension", 
      ylab="Y grid dimension", col = "blue")
      
      lines(xCurve, yCurve, type = "l", col = "black", lwd = 5)
      @

    \end{subfigure}
  \end{tabular}

  \caption{The meshes generated by the adaptive grid implementation I wrote
  using different strategies to assign points to intervals. Each mesh uses the
  same number of points, but arranges them differently.}\label{fig:adaptive-grids}
\end{figure*}

Developing a robust code to identify all the necessary parameters to divide the 
grid appropriately was challenging. However, even after the code was completed,
we not able to achieve the high order accuracy that we expected. In figure
\ref{fig:adaptive-convergence}, all three curves use exactly the same method to
compute the intervals over which the integrals are taken; however, the order of
accuracy is very different.

\begin{figure}[t]\centering
  <<adaptive-grid, out.width='0.65\\columnwidth'>>= 
  par( cex = 1.7 )

  dataFF <- read.table("../data/fixedfixed.dat", header=TRUE)
  dataFP <- read.table("../data/fixedscaled.dat", header=TRUE) 
  dataPP <- read.table("../data/scaledscaled.dat", header=TRUE)

  plot(dataFF, type = "o", log = "xy", 
  main = "Colton \\& Kress \n Adaptive Grid Convergence", 
  xlab="Number of points along each dimension (min 1)", 
  ylab="Absolute error with analytical integral", col = "blue", 
  xaxs='i', yaxs='r')

  lines(dataFP, type = "o", col = "green")

  lines(dataPP, type = "o", col = "red")
  
  legend("bottomleft", y=1, c("x evenly - y evenly", "x evenly - y scaled", 
  "x scaled - y scaled"), cex=.8, 
  col=c("blue","green", "red"), pch=c(1,1))
  @
  \caption{The convergence of the adaptive grid method I implemented using 
  different stratgies to assign points to the intervals.}\label{fig:adaptive-convergence}
\end{figure}

The difference in accuracy can be attributed to the way we assign discretization
points to the intervals in the domain. There are two strategies for assigning
points that we tried: scaling the number of points based on interval length and
dividing points evenly among intervals \emph{without} considering length. In
figure \ref{fig:adaptive-grids}, the curves with ``evenly x and/or y'' attribute
assigned points in the x or y dimension without regard to interval length. For
example, if the user wants 100 points in the x dimension and the code divides
the region into 4 intervals, then each interval gets 25 points (even if some of
the intervals are much smaller than others). This can be seen by the variation 
in the spacing of grid points in figure \ref{fig:adaptive-grids}.

Using the evenly divided strategies in both x and y surprisingly gives the best
convergence results. We are able to justify this result, but it was not
obvious. Our reasoning is the error in the scaled spacing method is bounded
below by the interval with the lowest points per unit length. If short intervals
are assigned points according to there length, then large parts of the integral
may not be resolved well since larger intervals are the first to receive new
points. By evenly distributing points, we ensure the error is bound above by the
largest intervals, which receive points at the same rate at small intervals;
ensuring convergence.

\subsection{Visualization 2D \& 3D Waveguides}

\begin{figure}[t]\centering
  \includegraphics[width=0.75\columnwidth]{images/3d_waveguide/waveguide}
  \caption{A denomstration of the visualization capabilities of our group using
    the codes I wrote to create a 3d model of a wave propagating through a 
    dielectric waveguide}\label{fig:cool-waveguide}
\end{figure}

The next part of the project will involve computing the fields (transmitted and
scattered) in a 3-dimensional domain. Furthermore, the domain will be a
dielectric so the field will extend to infinity outside the waveguide unlike the
PEC case. In order to make debugging easier and to help improve our figures for
future publications, I spent time programming tools and helper code to visualize
the field in a 3d domain.

I targeted the free and open source software Visit (developed by LLNL) which is
a visualization tool for 2d and 3d domains. In order to plot in Visit, the data
must be outputted in one of a few particular formats. To avoid wasting disk
space and time writing to disk, I wrote a MATLAB Executable (MEX) file which
takes a Matlab array and efficiently writes it to disk in a binary format
(.silo) to save space and time. This allows us to write very large 3d domains to
a format readable by Visit quickly. Furthermore, creating a MATLAB interface is
useful because most of the code for this project has been written in MATLAB
already, so there will be no need to change our existing codebase.

Once I completed the MEX file for writing the field to disk, I also wrote a
simple MATLAB code to create a mesh for the boundary of the waveguide. Combined
with my previous MEX files, we can now visualize the boundary of the waveguide
and the field propagating in and around it (figure
\ref{fig:cool-waveguide}). This will be very helpful for debugging and for
creating figures for publication.


