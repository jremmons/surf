This project presents several challenges: due to the open nature of dielectric
waveguides (i.e. the fields extend to infinity), the complete set of modes
include a continuous spectrum that makes the mode decomposition a mix of an
integral with unknown amplitude function, in addition to the tradition sum of
modes with unknown amplitude coefficients. There is no general approach used to
discretize the continuous spectrum for arbitrary geometries. Developing a method
to properly discretize these mode was a key component of this work. 

Another challenge in the simulation of such problems is the treatment of the
discontinuous permittivity along the boundary of the waveguide. Computing
projections onto the mode required 2D integrals in the plane of the waveguide;
since there are discontinuities, the spatial order of accuracy would be first
order (even for high-order methods) if no special considerations are made.
