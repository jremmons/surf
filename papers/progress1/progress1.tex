\documentclass[12pt,authoryear]{elsarticle}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\makeatletter % remove preprint footer
\def\ps@pprintTitle{%
 \let\@oddhead\@empty
 \let\@evenhead\@empty
 \def\@oddfoot{}%
 \let\@evenfoot\@oddfoot}
\makeatother

% \documentclass[preprint,12pt,authoryear]{elsarticle}

% R CONFIGURATION
% set R global options in opts_chunk$set(...)
%
% dev   : what output device to use (tex, png, pdf, etc) 
% echo  : output code used to generate the figure
% cache : only update figure if the embedded R code changed
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tex preamble
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[activate={true,nocompatibility},final,tracking=true,kerning=true,
  spacing=true,factor=1100,stretch=50,shrink=50]{microtype}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{ulem}

\usepackage{lipsum}

\newcommand{\vect}[1]{\boldsymbol{\mathbf{#1}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}
\begin{frontmatter}

%% Title, authors and addresses

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for theassociated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for theassociated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for theassociated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}

\title{Illumination of Dielectric Waveguides of Arbitrary Transverse Shape\\
  {\small Progress as of \today}
}
\journal{Computational Physics}

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{}
%% \address[label1]{}
%% \address[label2]{}

\author{John R. Emmons, Emmanuel Garza, and Oscar Bruno}
\address{California Institute of Technology, Applied and Computational Mathematics\\
1200 East California Boulevard, Pasadena, CA 91125}

\begin{abstract}
      Metallic and dielectric waveguides (e.g. optical fibers) form the backbone
      of modern long-distance communication and serve essential roles in
      scientific and industrial applications. The usefulness of waveguides is
      coupled closely with the ability of scientists and engineers to
      efficiently excite arbitrary optical modes of the waveguide. However, many
      important electromagnetic properties of waveguides cannot be found exactly
      for complex boundaries (i.e. shape of the waveguide); thus, continued
      advancement is dependent on numerical methods which provide accurate
      approximations. The aim of this project is to develop and characterize a
      robust and high-order numerical method to analyze the excitation of
      optical modes with an arbitrary incident field using a physically
      realistic model. This method will be based on the expansion of the field
      into eigenmodes inside the waveguide, while enforcing an integral equation
      based methodology for the fields in the surrounding region.
\end{abstract}
\end{frontmatter}

\section{Introduction}\vspace{-.25cm}

Metallic and dielectric waveguides (e.g. optical fibers) direct electromagnetic
waves to travel along their length. The propagation of electromagnetic waves
inside a waveguide is often described approximately using a geometric optics
approximation in which only the center of energy of the wave is
tracked. However, waveguides with diameters near the wavelength of a propagating
signal cannot be accurately described by this approximate geometric model;
rather, their electromagnetic properties (e.g. optical modes) must be analyzed
by finding solutions to Maxwell's equations.

In most cases, it is impossible to find closed-form solutions for waveguides
with complex boundaries/shapes. Hence, numerical methods must be used to
approximate the solutions of these problems. Developing accurate numerical
methods for simulating the propagation of electromagnetic fields inside and
outside of a dielectric waveguide is essential for continued advancement in the
design of specialized devices such as dielectric antennas with complicated
geometries.

Previous work in this area has focused only on circular waveguides
\cite{Snyder1969}, where solutions have a closed form expression. Even for the
circular case, it is typical to do certain approximations (e.g. assuming that
there is a small dielectric difference between the core and cladding, which
leads to ignoring the reflected field at interfaces). For the case of 2D
waveguides, a more complete set of theory and methods exists since there exist
an analytic expression for the modes of this type of planar waveguides
\cite{magnanini}, but the generalization to three dimensions is not
trivial. Modes for 3D waveguides depend on the geometry and there only exist
closed form expressions for highly symmetric waveguides.

Even more recent efforts still focus on radially symmetric
waveguides\cite{Alexandrov2003}. On the other hand, little attention has been
given to arbitrary shaped 3D waveguides beyond computing the field as an
expansion of bound modes \cite{Lu2012}; as a result, radiation away from the
core of the waveguide cannot be modeled. Other methods that take into account
the fields away from the core usually suffer from different
limitations. Previous works introduce a boundary in order to truncate the domain
\cite{Ciraolo} which leads to artificial reflections that do not correspond to
the real physical behavior of the fields.

The goal of this project is to develop and characterize a robust numerical
method to analyze the excitation of optical modes (bounded and unbounded) in a
dielectric waveguide with an arbitrary incident field. We will use a physically
realistic model which unlike previous works
\cite{Ciraolo,Lu2012,Alexandrov2003}, does not introduce approximations to the
domain or limit use to symmetric systems. This method will expand the field into
eigenmodes inside the waveguide and use integral equation methods for the fields
in the surrounding exterior region.

% \subsection{Physical Description}
% knit_child('sections/physical_description.Rnw')

\section{Challenges}

This project presents several challenges: due to the open nature of dielectric
waveguides (i.e. the fields extend to infinity), the complete set of modes
include a continuous spectrum that makes the mode decomposition to be a mix of
an integral with unknown amplitude function, in addition to the tradition sum of
modes with unknown amplitude coefficients.

However, the main challenge in the simulation of such problems is the treatment
of the discontinuous permittivity, since it reduces the spatial order of
accuracy to first order (even for high-order methods) if no special
considerations are made. The aim of the project is to find ways to overcome this
difficulty.

\section{Objectives}

In this project, we seek to develop an accurate methodology for solving the
problem of illumination of a dielectric waveguide of arbitrary transverse shape
by an arbitrary incident electromagnetic field (figure
\ref{fig:illumination}). This work extends current efforts by Professor's Bruno
group related to mode expansions of dielectric waveguides of arbitrary
geometries. Currently, an efficient way to find the modal fields exists, however
it is still uncertain as to what is the best numerical approach in order to use
these modal fields for solving the problem of arbitrary illumination.

\begin{figure}[t]\centering
  \includegraphics[scale=0.5]{images/illumination_waveguide/illumination}
  \caption{Graphical depiction of the illumination problem. Given a known
    incident field that illuminates the waveguide, we seek to find the reflected
    and transmitted fields.}\label{fig:illumination}
\end{figure}

The problem of illumination of an arbitrary shape waveguide with known modal
functions carries out two main difficulties: the first is to compute accurately
the modal amplitudes (coefficients for the discrete spectrum and functions for
the continuous part of the spectrum); and the second challenge is to account for
the reflected field. Our objective is to solve both these challenges by the end
of the summer.

\section{Progress: (as of \today)}


\subsection{Mode Matching for PEC in 2D \& 3D}

In the first week, I implemented a code to take an arbitrary expansion of modes
(i.e. an EM wave) in a perfect electrical conductor (PEC) planar waveguide and
propagate this expansion (wave) past a discontinuity. In this particular
exercise, the waveguide (width $a$) was split into two smaller waveguides (width
$b$ and $c$ such that $a = b + c$) by a perfect barrier. In order to do this, I
first solved (analytically) for the modes of a rectangular PEC waveguide in 2
and 3 dimensions. The solution for this was already known and can be readily
found in many introductory electromagnetic theory text books. However, deriving
the expressions starting with only Maxwell's Equations was a good exercise for
reviewing the basics.

Once I had derived the expressions for the modes, I implemented a simple code in
MATLAB to propagate and visualize the fields in time. The final step was to
solve for the mode coefficients in the waveguides of width $b$ and $c$ given the
coefficients in the waveguide of width $a$. This was done by solving a linear
system of equations after applying the mode matching procedure.

\subsection{Integral Equation Methods for 2D Scattering}

\begin{figure}[t]\centering
  
  \begin{tabular}{cc}
    \begin{subfigure}[b]{0.5\columnwidth}
      \includegraphics[height=6.25cm]{images/integral_equations/rocket}
    \end{subfigure}

    &

    \begin{subfigure}[b]{0.5\columnwidth}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\columnwidth]{figure/ie-error-1} 

\end{knitrout}
    \end{subfigure}
  \end{tabular}
  
  \caption{[left] A field coming from the $+x$ axis hits a 2D PEC kite and
    scatters; [right] the integral equation methods used to produce this simulation 
    converge spectrally to machine precision (double precision).}\label{fig:rocket}
\end{figure}

An integral equation is an equation where the function of interest appears 
under an integral sign. For example,

\begin{equation}
  f(x) = \int K(x,t)\,\varphi(t)\,dt. 
\end{equation}

\noindent
is an integral equation if $f(x)$ and $\varphi(t)$ and known and the function of
interest is $K(x,t)$. To solve these problems numerically, we discretize the
variable $x$, then solve the linear systems of equations that is created by
approximating the integral.

These methods have many advantages over, simpler methods; for example, finite
difference methods must spatially discretize a domain; however, this is
nontrivial for problems where the domain is infinite. In the case of scattering,
the fields extend to infinity everywhere, so special consideration would have to
be taken to solve these problems with finite differences. With integral
equations though, we do not discretize the domain spatially; rather, we
discretize along the boundary of the scattering object. No special consideration
is required to account for truncatation or fields which do not decay rapidly.

For scattering, the unknown function is the electron density along the boundary
of the PEC object. It is easy to compute the field everywhere once the density
along the boundary is known, so my goal was to write a code to find this density
using integral equations. Figure \ref{fig:rocket} (on the left), shows the
results of my work for a kite shaped boundary. The field is plotted around the
object in black with an incident field from the $+x$ axis.

On the right of figure \ref{fig:rocket}, I show the convergence of the integral
equation methods for the scattering problem for a cylinder (for which the
analytical solution is easy to compute). Note that the error between the
numerical and analytical solution decreases super-linearly as the number of mesh
points along the boundary increases. 

By implementing an integral equations method for the 2D scattering problem, I
learned how to apply integral equations to more complex problems. The next step
will be to use integral equations to compute the densities in a 3D and
dielectric domain.

\subsection{Projections for 3D Dielectric Waveguide of Arbitrary Shape}

In order to compute the projection of an arbitrary field onto the modes of the 
waveguide, we need to be able to compute 2-dimensional integrals with very high
order accuracy quickly. This is a non-trivial task in our case since the field
is discontinuous at the boundary of the interior and exterior of the waveguide.
As a result, we cannot use simple methods (such as the trapezoid method) to 
preform these integrals because they produce low order accuracy when there are
discontinuities in the function. 

To address this issue, I helped to design and implement a generic 2d integration
method which is high order regardless of the shape of the waveguide boundary.
The main idea is to divide the region of integration in such as way as to avoid
integrating over any discontinuities, then use high order methods for each
smaller interval. To divide the region, I first identify locations along the
curve where the x derivative of the parameterization is zero (i.e where the
curve goes straight up or down in figure \ref{fig:adaptive-grids}). These points
represents locations along the x-dimension where the function could have a
discontinuous jump. I then discretize these intervals using a high order method
to obtain the location of the columns (i.e. x grid). The next step is to
identify where the columns intersect the curve and create grids in a similar way
as before in the y dimension (creating the y grid). Combining these two, we have
a non-uniform grid in x and y which avoids discontinuities and achieves high
order accuracy.

\begin{figure*}[t]\centering
  
  \begin{tabular}{cc}

    \begin{subfigure}[b]{0.5\columnwidth}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\columnwidth]{figure/gridPP-1} 

\end{knitrout}

    \end{subfigure}

    &

    \begin{subfigure}[b]{0.5\columnwidth}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\columnwidth]{figure/gridFF-1} 

\end{knitrout}

    \end{subfigure}
  \end{tabular}

  \caption{The meshes generated by the adaptive grid implementation I wrote
  using different strategies to assign points to intervals. Each mesh uses the
  same number of points, but arranges them differently.}\label{fig:adaptive-grids}
\end{figure*}

Developing a robust code to identify all the necessary parameters to divide the 
grid appropriately was challenging. However, even after the code was completed,
we not able to achieve the high order accuracy that we expected. In figure
\ref{fig:adaptive-convergence}, all three curves use exactly the same method to
compute the intervals over which the integrals are taken; however, the order of
accuracy is very different.

\begin{figure}[t]\centering
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=0.65\columnwidth]{figure/adaptive-grid-1} 

\end{knitrout}
  \caption{The convergence of the adaptive grid method I implemented using 
  different stratgies to assign points to the intervals.}\label{fig:adaptive-convergence}
\end{figure}

The difference in accuracy can be attributed to the way we assign discretization
points to the intervals in the domain. There are two strategies for assigning
points that we tried: scaling the number of points based on interval length and
dividing points evenly among intervals \emph{without} considering length. In
figure \ref{fig:adaptive-grids}, the curves with ``evenly x and/or y'' attribute
assigned points in the x or y dimension without regard to interval length. For
example, if the user wants 100 points in the x dimension and the code divides
the region into 4 intervals, then each interval gets 25 points (even if some of
the intervals are much smaller than others). This can be seen by the variation 
in the spacing of grid points in figure \ref{fig:adaptive-grids}.

Using the evenly divided strategies in both x and y surprisingly gives the best
convergence results. We are able to justify this result, but it was not
obvious. Our reasoning is the error in the scaled spacing method is bounded
below by the interval with the lowest points per unit length. If short intervals
are assigned points according to there length, then large parts of the integral
may not be resolved well since larger intervals are the first to receive new
points. By evenly distributing points, we ensure the error is bound above by the
largest intervals, which receive points at the same rate at small intervals;
ensuring convergence.

\subsection{Visualization 2D \& 3D Waveguides}

\begin{figure}[t]\centering
  \includegraphics[width=0.75\columnwidth]{images/3d_waveguide/waveguide}
  \caption{A demonstration of the visualization capabilities of our group using
    the codes I wrote to create a 3d model of a wave propagating through a 
    dielectric waveguide}\label{fig:cool-waveguide}
\end{figure}

The next part of the project will involve computing the fields (transmitted and
scattered) in a 3-dimensional domain. Furthermore, the domain will be a
dielectric so the field will extend to infinity outside the waveguide unlike the
PEC case. In order to make debugging easier and to help improve our figures for
future publications, I spent time programming tools and helper code to visualize
the field in a 3d domain.

I targeted the free and open source software Visit (developed by LLNL) which is
a visualization tool for 2d and 3d domains. In order to plot in Visit, the data
must be outputted in one of a few particular formats. To avoid wasting disk
space and time writing to disk, I wrote a MATLAB Executable (MEX) file which
takes a Matlab array and efficiently writes it to disk in a binary format
(.silo) to save space and time. This allows us to write very large 3d domains to
a format readable by Visit quickly. Furthermore, creating a MATLAB interface is
useful because most of the code for this project has been written in MATLAB
already, so there will be no need to change our existing codebase.

Once I completed the MEX file for writing the field to disk, I also wrote a
simple MATLAB code to create a mesh for the boundary of the waveguide. Combined
with my previous MEX files, we can now visualize the boundary of the waveguide
and the field propagating in and around it (figure
\ref{fig:cool-waveguide}). This will be very helpful for debugging and for
creating figures for publication.



%\section{Method}
%knit_child('sections/method.Rnw')

%\section{Algorithm/Implementation}
%knit_child('sections/algorithm.Rnw')

\section{Work Plan}


\begin{itemize}
\item \sout{ Week 1: Introduction - Solve model problems for numerical integration,
  mode-matching problems and integral equations. }
\item \sout{ Week 2-3: Solve the illumination problem for the 2D dielectric planar
  waveguide case. }
\item \sout{ Weeks 4-5: Implement the 2D numerical integration method based on the
  windowing function in order to compute the modal amplitudes. }
\item Weeks 6-7: Solve the 3D illumination problem by ignoring the reflected
  field (approximate the transmitted field by the incident field).
\item Weeks 7-10: Implement a mode-matching scheme in order to account for the
  reflected fields.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\nocite{*}
%\bibliographystyle{plain}
\bibliographystyle{abbrv}
\bibliography{progress1} 

\end{document}
