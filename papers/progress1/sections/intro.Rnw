Metallic and dielectric waveguides (e.g. optical fibers) direct electromagnetic
waves to travel along their length. The propagation of electromagnetic waves
inside a waveguide is often described approximately using a geometric optics
approximation in which only the center of energy of the wave is
tracked. However, waveguides with diameters near the wavelength of a propagating
signal cannot be accurately described by this approximate geometric model;
rather, their electromagnetic properties (e.g. optical modes) must be analyzed
by finding solutions to Maxwell's equations.

In most cases, it is impossible to find closed-form solutions for waveguides
with complex boundaries/shapes. Hence, numerical methods must be used to
approximate the solutions of these problems. Developing accurate numerical
methods for simulating the propagation of electromagnetic fields inside and
outside of a dielectric waveguide is essential for continued advancement in the
design of specialized devices such as dielectric antennas with complicated
geometries.

Previous work in this area has focused only on circular waveguides
\cite{Snyder1969}, where solutions have a closed form expression. Even for the
circular case, it is typical to do certain approximations (e.g. assuming that
there is a small dielectric difference between the core and cladding, which
leads to ignoring the reflected field at interfaces). For the case of 2D
waveguides, a more complete set of theory and methods exists since there exist
an analytic expression for the modes of this type of planar waveguides
\cite{magnanini}, but the generalization to three dimensions is not
trivial. Modes for 3D waveguides depend on the geometry and there only exist
closed form expressions for highly symmetric waveguides.

Even more recent efforts still focus on radially symmetric
waveguides\cite{Alexandrov2003}. On the other hand, little attention has been
given to arbitrary shaped 3D waveguides beyond computing the field as an
expansion of bound modes \cite{Lu2012}; as a result, radiation away from the
core of the waveguide cannot be modeled. Other methods that take into account
the fields away from the core usually suffer from different
limitations. Previous works introduce a boundary in order to truncate the domain
\cite{Ciraolo} which leads to artificial reflections that do not correspond to
the real physical behavior of the fields.

The goal of this project is to develop and characterize a robust numerical
method to analyze the excitation of optical modes (bounded and unbounded) in a
dielectric waveguide with an arbitrary incident field. We will use a physically
realistic model which unlike previous works
\cite{Ciraolo,Lu2012,Alexandrov2003}, does not introduce approximations to the
domain or limit use to symmetric systems. This method will expand the field into
eigenmodes inside the waveguide and use integral equation methods for the fields
in the surrounding exterior region.
