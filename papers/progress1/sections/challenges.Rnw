This project presents several challenges: due to the open nature of dielectric
waveguides (i.e. the fields extend to infinity), the complete set of modes
include a continuous spectrum that makes the mode decomposition to be a mix of
an integral with unknown amplitude function, in addition to the tradition sum of
modes with unknown amplitude coefficients.

However, the main challenge in the simulation of such problems is the treatment
of the discontinuous permittivity, since it reduces the spatial order of
accuracy to first order (even for high-order methods) if no special
considerations are made. The aim of the project is to find ways to overcome this
difficulty.
