%module inplace

%{
    #define SWIG_FILE_WITH_INIT
    #include "ex.h"
%}

%include "numpy.i"

%init %{
    import_array();
%}


%apply (double* IN_ARRAY1, int DIM1) {(double* in, int n)};
%include "ex.h" 
