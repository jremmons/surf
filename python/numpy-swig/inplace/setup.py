#! /usr/bin/env python

from distutils.core import setup, Extension
from distutils.command.build import build

import numpy

class CustomBuild(build):
    sub_commands = [
        ('build_ext', build.has_ext_modules), 
        ('build_py', build.has_pure_modules),
        ('build_clib', build.has_c_libraries), 
        ('build_scripts', build.has_scripts),
    ]

try:
    numpy_include = numpy.get_include()
except AttributeError:
    numpy_include = numpy.get_numpy_include()

_inplace = Extension('_inplace',
                     sources=['inplace/inplace.i',
                              'inplace/inplace.c'],
                     include_dirs = [numpy_include],
                     extra_compile_args = ['-fopenmp'], 
                     extra_link_args = ['-lgomp'],
                 )

setup(  name = "inplace",
        cmdclass = {'build' : CustomBuild },
        packages = ['inplace'],
        py_modules = ['inplace/inplace'], 
        ext_modules = [_inplace]
    )
