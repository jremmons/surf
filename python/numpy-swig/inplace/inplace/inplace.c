#include <stdio.h>

#include <omp.h>
#include <math.h>
#include "inplace.h"

void inplace(double *invec, int n){
    int i;
    for (i=0; i<n; i++){
      invec[i] = 2*invec[i];
    }
}

void inplace2(int n, int m, double* in){

  omp_set_num_threads(4);
  omp_set_dynamic(0);

  double (*mat)[m] = (double (*)[m]) in;

  int i, j;
  #pragma omp parallel for collapse(2)
  for (i = 0; i < n; i++){

    for(j = 0; j < m; j++){
      double tmp = mat[i][j];
      mat[i][j] = 2 * pow(tmp, tmp);
    }

  }

}

