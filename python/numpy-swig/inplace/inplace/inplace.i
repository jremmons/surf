%module inplace

%{
    #define SWIG_FILE_WITH_INIT
  void inplace(double *INPLACE_ARRAY1, int DIM1);
  void inplace2(int DIM1, int DIM2, double* INPLACE_ARRAY2);
//    #include "inplace.h"
%}

%include "numpy.i"

%init %{
    import_array();
%}

void inplace(double *INPLACE_ARRAY1, int DIM1);
void inplace2(int DIM1, int DIM2, double* INPLACE_ARRAY2);
