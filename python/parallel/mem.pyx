import os
import ctypes
import numpy as np
import multiprocessing as mp

SIZE = 10000

def compute(idx):
    var = 0
    for i in xrange(1000):
        for j in xrange(50):
            var += idx*(i*j)

    data[i] = var
        
def init(_data):
    global data
    data = _data

def main():
    
    # create shared memory
    shared_arr = mp.Array(ctypes.c_double, SIZE, lock=False)
    data = np.frombuffer(shared_arr, dtype=ctypes.c_double)
    init(data)
    
    # intialize array with some values
    for i in range(SIZE):
        data[i] = 0

    # create a cpu pool
    num_cpus = mp.cpu_count()
    print "cpu count:", num_cpus
    cpu_pool = mp.Pool(processes=num_cpus, initargs=(data))

    # run operation on the array in parallel
    cpu_pool.map(compute, range(SIZE))

    # output the array
    #for i in range(SIZE):
    #    print data[i]

if __name__ == '__main__':
    main()
    
