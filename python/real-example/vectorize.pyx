#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

# function to vectorize
def myfun(a, b):
    return mult(a, b)

# c code!
cdef double mult(double a, double b):
    return a*a + b*b;

def example():
    DIM = 100

    xmin = -5
    xmax = 5
    ymin = -5
    ymax = 5

    x = np.linspace(xmin, xmax, DIM)
    y = np.linspace(ymin, ymax, DIM)

    X, Y = np.meshgrid(x, y)

    vfun = np.vectorize(myfun)
    c = vfun(X, Y)

    plt.pcolormesh(x, y, c)
    plt.axis([xmin, xmax, ymin, ymax])
    plt.show()
