import distutils.core as dist
import Cython.Build as cy
import os

# use intel compiler
os.environ["CC"] = "icc"
os.environ["CXX"] = "icpc"

dist.setup(
  name = 'vectorize',
  ext_modules = cy.cythonize("vectorize.pyx")
)
