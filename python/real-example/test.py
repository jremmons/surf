#!/usr/bin/env python

import os
import sys

os.environ["PYTHONPATH"] = 'build/lib.linux-x86_64-2.7'
print os.system('echo $PYTHONPATH')

if len(sys.argv) == 2:
    os.system('python setup.py clean')
    
else:
    os.system('python setup.py build')
    import vectorize as v
    v.example()
