#include <stdio.h>
#include <omp.h>
#include <math.h>

int hello(){
  
  printf("hello!\n");

  omp_set_dynamic(0);
  omp_set_num_threads(4);
  #pragma omp parallel for
  for(int i = 0; i < 100000000; i++){    
    double x = sqrt(i) * sqrt(i);
    double y = pow(i, i);
    double z = sqrt(sin(pow(i,i)));
  }

  #pragma omp parallel 
  {
    printf("Num threads: %i\n", omp_get_num_threads());
    printf("Thread num: %i\n", omp_get_thread_num());
  }

  return 199;
  
}
