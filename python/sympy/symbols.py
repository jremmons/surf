import sympy as sp

x = sp.Symbol('x')
y = sp.Symbol('y')

f = x**2 - 3*x - 1

print f
print sp.diff(f, x, 1)
