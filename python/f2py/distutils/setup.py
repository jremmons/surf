#!/usr/bin/env python

from numpy.distutils.core import Extension
from numpy.distutils.core import setup

ext = Extension(name = 'scalar',
                sources = ['scalar/scalar.f90', 'scalar/mod1.f90']
            )


setup(name = 'example',
      ext_modules = [ext],
  )


