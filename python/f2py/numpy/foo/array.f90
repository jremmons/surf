module array  

  contains

    subroutine test(a, n, m)
      use mod2

      integer n,m,i,j
      real*8 A(n,m)
      
      do j = 1, m
         a(1,j) = A(1,j) + 20D0
      enddo

      do i = 1, n
         a(i,1) = A(i,1) - 20D0
      enddo
      
      call mod2_test(n)
      call mod2_test(m)
      
    end subroutine test

end module array
