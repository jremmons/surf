
subroutine foo(a,n,m)
  use array

  integer n,m
  real*8 A(n,m)
  !f2py intent(in, out, copy) a
  !f2py integer intent(hide),depend(a) :: n=shape(a,0), m=shape(a,1)

  call test(a, n, m)

end subroutine FOO
