#!/usr/bin/env python

from numpy.distutils.core import Extension
from numpy.distutils.core import setup

ext = Extension(name = '_foo',
                sources = ['foo/foo.f90', 
                           'foo/mod2.f90',
                           'foo/array.f90']
            )

setup(name = 'foolib',
      ext_modules = [ext],
      py_modules = ['foo/foo']
  )


