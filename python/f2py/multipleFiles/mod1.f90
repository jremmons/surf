! Fortran 90 assignment 1
module mymod

  integer, parameter :: const1 = 123

contains 

  function func(i) result(out)
    integer, intent(in)  :: i
    integer              :: out
    ! return value
    out = i+i+const1
  end function func

  subroutine test(M)
      integer :: M(:,:)
      integer :: len
      integer :: i,j

      len = ubound(M, 1)

      ! create identity matrix
      do i=1,len
         do j=1,len
            
            if( i == j) then
               M(i,j) = 1
            else
               M(i,j) = 0
            end if

         end do
      end do

  end subroutine

end module mymod
