! Fortran 90 assignment 1

SUBROUTINE hello(x,y)
  use mymod
  integer :: x,y
  integer :: i,j,len
  integer :: A(10,10)

  ! prompt the user
  print *, "Enter two numbers:"

  ! read in the values
  !read *, x
  !read *, y

  call test(A)

  len = ubound(A,1)

  do i=1,len
     do j=1,len
        write (*,"(I5)",advance='no') A(i,j)
     end do
     write (*,*)
  end do


  print *, "The total is:", func(x+y)

end subroutine hello
