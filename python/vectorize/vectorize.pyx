#!/usr/bin/env python
import numpy as np

# function to vectorize
def myfun(a, b):
    return mult(a, b)

# c code!
cdef int mult(int a, int b):
    return a * b + 1;

def example():

    vfun = np.vectorize(myfun)
    
    a = np.array([[1, 2, 3], [4, 5, 6]], np.int32)
    b = np.array([[1, 2, 3], [4, 5, 6]], np.int32)

    print vfun(a,b)

