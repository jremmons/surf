#!/usr/bin/env python

from numpy.distutils.core import Extension
from numpy.distutils.core import setup

ext = Extension(name = '_print_john',
                sources = ['src/print_helper.f90', 
                           'src/print.f90']
            )

setup(name = 'print_john',
      ext_modules = [ext],
      package_dir = {'' : 'src'},
      py_modules = ['jprint']
  )


