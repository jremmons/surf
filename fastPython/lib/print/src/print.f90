
subroutine print_john(x)
      use print_helper, only : helper

      integer :: x
      if( x .ne. 7 ) then
         print*, "John likes the number ", x
      else
         call helper(x)
      endif

      call printer(x)

end subroutine print_john

subroutine printer(x)
  integer :: x
  
  print*, "hello world from print_helper! ", x

end subroutine
