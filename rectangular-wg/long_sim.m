%
% Author: John Emmons 
% Description: This code plots a longitudial cross section of a rectangular
% waveguide with a TE wave propagating along its length.
%
clear all;

% define the parameters
width = 1;
height = 1;
length = .1;
x = 0.5;
c = 1;
w = 100;

MAX_COMPONENTS = 3;
MESH_POINTS_DIM_Y = 100;
MESH_POINTS_DIM_Z = 1000;

coeff = project(MAX_COMPONENTS);
[m_max, n_max] = size(coeff);

y_space = linspace(0,height,MESH_POINTS_DIM_Y);
z_space = linspace(0,length,MESH_POINTS_DIM_Z);
for t=0:.01:1

    long_s = zeros(MESH_POINTS_DIM_Y, MESH_POINTS_DIM_Z);
    [Z,Y] = meshgrid(z_space, y_space);
    for m_i=1:m_max
        for n_i=1:n_max
            k = sqrt( (w/c)^2 - (pi^2)*( (m_i/width)^2 + (n_i/height)^2 ) );
                        
            long_s = long_s + ...
                     rect_mode(width, height, m_i-1, n_i-1, x, Y) .* ...
                     coeff(m_i, n_i) .* ...
                     exp(i*(k*Z - w*t));

        end
    end

    % plot
    pcolor(z_space, y_space, real(long_s));
    shading interp
    caxis([-1, 1])
    pause(.05)

end