%
% Author: John Emmons
% Description: This code plots a cross section of a rectangular waveguide
% with a TE wave propagating along its length.
%
clear all;

% define the parameters
width = 1;
height = 1;
z = 0; % z
c = 1;
w = 100;

MAX_COMPONENTS = 3;
MESH_POINTS_DIM_X = 100;
MESH_POINTS_DIM_Y = 100;

coeff = project(MAX_COMPONENTS);
[m_max, n_max] = size(coeff);

x_space = linspace(0,width,MESH_POINTS_DIM_X);
y_space = linspace(0,height,MESH_POINTS_DIM_Y);    
for t=0:.1:10

    cross_s = zeros(MESH_POINTS_DIM_Y, MESH_POINTS_DIM_X);
    [X,Y] = meshgrid(x_space, y_space);
    for m_i=1:m_max
        for n_i=1:n_max            
            k = sqrt( (w/c)^2 - (pi^2)*( (m_i/width)^2 + (n_i/height)^2 ) );

            cross_s = cross_s + ...
                      rect_mode(width, height, m_i-1, n_i-1, X, Y) * ...
                      coeff(m_i, n_i) * ...
                      exp(i*(k*z - w*t));
        end
    end

    % plot
    pcolor(x_space, y_space, real(cross_s));
    shading interp
    caxis([-1, 1])
    pause(.1)

end