function amp = rect_mode(a, b, m, n, x, y)

amp = cos(pi*m*x/a).*cos(pi*n*y/b);

end